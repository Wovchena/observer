﻿#include "Observer.h"
#include <Windows.h>
#include <iostream>


void Subject::Attach(Observer * observer) // attach the obs to subject
{
    attached_.insert(observer);
}

void Subject::Detach(Observer * observer)
{
    attached_.erase(observer);
}

void Subject::Notify()
{
    for (ObsIter it = attached_.begin(); it != attached_.end(); ++it) {
        Observer &obs = **it;
        obs.Update(this);
    }
}

int Timer::GetState()
{
    return time_spent_;
}
void Timer::Start()
{
    time_spent_ = 0;
    for (int i = 0; i < 50; ++i){
        Sleep(1000);
        time_spent_++;
        Notify();
    }
}

ProgressBar::ProgressBar(int seconds)
{
    required_time_ = seconds;
    observer_id_ = ++count_;
}

void ProgressBar::Update(Subject* subject)
{
    int time_spent = subject->GetState();
    if (time_spent <= required_time_)
    {
        char c = 219;                                 // код закрашенного знакоместа
        COORD position;                                             // Printing from required place
        HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);          // Printing from required place
        position.X = 0;                                             // Printing from required place
        position.Y = 40 - observer_id_ * 2;                         // Printing from required place
        SetConsoleCursorPosition(hConsole, position);               // Printing from required place
        std::cout << observer_id_ << ":\n";                         // Printing from required place
        for (int i = 0; i < time_spent * 80 / required_time_; ++i)  // Printing from required place
            std::cout << c;                                         // Printing from required place
    }
}

int ProgressBar::count_ = 0;

struct Client {
    void go()
    {
        Timer timer;
        ProgressBar progressBar(5);
        ProgressBar progressBar2(27);
        ProgressBar progressBar3(7);

        timer.Attach(&progressBar);
        timer.Attach(&progressBar2);
        timer.Attach(&progressBar3);
        timer.Start();
        timer.Detach(&progressBar3);
        timer.Detach(&progressBar2);
        timer.Detach(&progressBar);
    }

};

int main() {
    Client client1;
    client1.go();
    return 0;
}