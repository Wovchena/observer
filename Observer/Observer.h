#include <set>

class Subject;

class Observer
{
public:
    virtual void Update(Subject *) = 0;
};

class Subject
{
private:
    std::set<Observer*> attached_;
    typedef std::set<Observer*>::iterator ObsIter;

public:
    virtual void Attach(Observer * observer); // attach the obs to subject
    virtual void Detach(Observer * observer);
    virtual void Notify();
    virtual int GetState() = 0;
};

class Timer : public Subject
{
private:
    int time_spent_;
public:
    void Start();
    int GetState();
};

class ProgressBar : public Observer
{
private:
    int required_time_;
    int observer_id_;                 // serial number of the observer
public:
    static int count_;
    ProgressBar(int seconds);
    void Update(Subject* subject);
};